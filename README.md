# Some information about the solution

- IDE: MS Visuaa Studio 2019.
- Backend language: C# (.Net Framework).
- Architecture: MVC (ASPNET MVC).
- Frontend language and framworks: javascript, JQuery.
- CSS framework: bootstrap.


Note:  On the main page, you can choose the SD File that you wish to parse. Then clicking on "Parse SD File" button the records (Compound) present in SD File will be shown in the "Records
" section. Once the records have been shown, you can select any record and its properties and graphical representation will be displayed in sections "Record Properties" and "Record Image" 
respectively. Finally, in section "Record Properties" you can search for properties that match with a given pattern (enter the pattern in "Search" input and click on the "Search" button).

