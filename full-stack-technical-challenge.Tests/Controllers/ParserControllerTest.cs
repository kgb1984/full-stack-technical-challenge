﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using full_stack_technical_challenge.Controllers;
using full_stack_technical_challenge.Tests.Utilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace full_stack_technical_challenge.Tests.Controllers
{
    [TestClass]
    public class ParserControllerTest
    {
        //Tests the result of Parse Action is not null for a valid SD File
        [TestMethod]
        public void Parse_NotNullResult()
        {
            // Arrange
            ParserController controller = new ParserController();
            string fileName = Path.Combine(Directory.GetCurrentDirectory() , @"..\..\TestFiles\sample_1.sdf");
            HttpPostedFileBase inputFile = new MemoryFile(new FileStream(fileName, FileMode.Open), fileName) as HttpPostedFileBase;

            // Act
            JsonResult result = controller.Parse(inputFile) as JsonResult;

            // Assert
            Assert.IsNotNull(result);
        }

        //Tests when invalid SD File has been selected
        [TestMethod]
        public void Parse_InvalidSDFile()
        {
            // Arrange
            ParserController controller = new ParserController();
            string fileName = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\TestFiles\sample_1.json");
            HttpPostedFileBase inputFile = new MemoryFile(new FileStream(fileName, FileMode.Open), fileName) as HttpPostedFileBase;

            // Act
            JsonResult result = controller.Parse(inputFile) as JsonResult;

            // Assert
            Assert.AreEqual(result.Data, "Please select a valid file");
        }

        //Tests that the selected SD File does not have a valid format
        [TestMethod]
        public void Parse_InvalidSDFileFormat()
        {
            // Arrange
            ParserController controller = new ParserController();
            string fileName = Path.Combine(Directory.GetCurrentDirectory(), @"..\..\TestFiles\corrupted.sdf");
            HttpPostedFileBase inputFile = new MemoryFile(new FileStream(fileName, FileMode.Create), fileName) as HttpPostedFileBase;

            // Act
            JsonResult result = controller.Parse(inputFile) as JsonResult;

            // Assert
            Assert.AreEqual(result.Data, "The selected SD File does not have a valid format.");
        }


    }
}
