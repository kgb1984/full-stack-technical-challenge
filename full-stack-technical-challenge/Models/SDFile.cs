﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace full_stack_technical_challenge.Models
{
    public class SDFile
    {
        public List<Compound> Compounds { get; }
    }
}