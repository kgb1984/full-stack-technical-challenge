﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace full_stack_technical_challenge.Models
{
    public class Compound
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public Molfile Molfile { get; set; }
        public List<DataItem> DataItems { get;}
    }
}