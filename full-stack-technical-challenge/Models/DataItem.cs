﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace full_stack_technical_challenge.Models
{
    //Class to model Compound's data item
    public class DataItem
    {        
        public string Property { get; set; }
        public string Value { get; set; }
        
    }
}