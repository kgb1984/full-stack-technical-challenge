﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace full_stack_technical_challenge.CustomException
{
    public class InvalidSDFileFormatException : FormatException
    {
        public const string MSG = "The selected SD File does not have a valid format.";

        public InvalidSDFileFormatException(string message) : base(message)
        {
        }
    }
}