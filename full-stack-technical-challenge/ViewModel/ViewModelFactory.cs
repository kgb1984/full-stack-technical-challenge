﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using full_stack_technical_challenge.CustomException;
using full_stack_technical_challenge.Models;

namespace full_stack_technical_challenge.ViewModel
{
    //Class responsible to create view-model objects
    public class ViewModelFactory
    {
        //Creates a CompoundViewModel object from string data representing a Compound
        internal static CompoundViewModel CreateCompoundViewModel(string compoundString)
        {
            CompoundViewModel compoundViewModel = new CompoundViewModel();
            try
            {
                compoundViewModel.MolFileStructure = compoundString.Substring(0, compoundString.IndexOf('>'));

                string[] dataItemArray = compoundString.Substring(compoundString.IndexOf('>')).Split(new string[] { Environment.NewLine + Environment.NewLine },
                   StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < dataItemArray.Length; i++)
                {
                    compoundViewModel.Properties.Add(CreateDataItem(dataItemArray[i]));
                }
                compoundViewModel.PropertiesCount = compoundViewModel.Properties.Count;
            }
            catch (Exception ex)
            {
                throw new InvalidSDFileFormatException(InvalidSDFileFormatException.MSG);
            }

            return compoundViewModel;
        }

        //Creates a DataItem object from string data representing an item
        private static DataItem CreateDataItem(string dataItemString)
        {
            DataItem dataItem = new DataItem();
            try
            {
                int start = dataItemString.IndexOf('<') + 1;
                dataItem.Property = dataItemString.Substring(start, dataItemString.LastIndexOf('>') - start);
                string[] dataArray = dataItemString.Substring(dataItemString.IndexOf(Environment.NewLine)).Split(new string[] { Environment.NewLine },
                   StringSplitOptions.RemoveEmptyEntries);
                foreach (string data in dataArray)
                {
                    dataItem.Value += data + " ";
                }
                dataItem.Value = dataItem.Value.Substring(0, dataItem.Value.Length - 1);
            }
            catch(Exception ex)
            {
                throw new InvalidSDFileFormatException(InvalidSDFileFormatException.MSG);
            }
          
            return dataItem;
        }
    }
}