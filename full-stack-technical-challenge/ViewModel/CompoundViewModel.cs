﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using full_stack_technical_challenge.Models;

namespace full_stack_technical_challenge.ViewModel
{
    //Class to model the graphical representation of a single Compound
    public class CompoundViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public int PropertiesCount { get; set; }        
        public string MolFileStructure { get; set; }
        public List<DataItem> Properties{ get; }

        public CompoundViewModel()
        {
            Properties = new List<DataItem>();
            Name = "Compound";
            ID = Guid.NewGuid().ToString();
        }

       
    }
}