﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace full_stack_technical_challenge.ViewModel
{
    //Class to model the graphical representation of a single SD File
    public class SDFileViewModel
    {
        public List<CompoundViewModel> CompoundViewModels { get; }

        public SDFileViewModel()
        {
            CompoundViewModels = new List<CompoundViewModel>();
        }

    }
}