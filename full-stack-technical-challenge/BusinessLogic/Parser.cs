﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using full_stack_technical_challenge.CustomException;
using full_stack_technical_challenge.Models;
using full_stack_technical_challenge.ViewModel;

namespace full_stack_technical_challenge.BusinessLogic
{
    //Class responsible to parse a single SD File to JSON
    public class Parser
    {
        public string PathFile { get; set; }
        public SDFileViewModel SDFileViewModel { get; set; }
       public Parser(string pathFile) 
        {
            this.PathFile = pathFile;
            SDFileViewModel = new SDFileViewModel();
            CreateSDFileViewModel();
           
        }

        /*Creates SDFile (view-model) from file in "PathFile"*/
        private SDFileViewModel CreateSDFileViewModel()
        {
            try
            {
                // Read the file as one string.
                string sdFileString = System.IO.File.ReadAllText(PathFile);
                string[] compoundStringArray = sdFileString.Split(new string[] { "$$$$" + Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries);
                foreach (string s in compoundStringArray)
                {
                    SDFileViewModel.CompoundViewModels.Add(CreateCompoundViewModel(s));

                }
            }
            catch(Exception ex)
            {
                throw new InvalidSDFileFormatException(InvalidSDFileFormatException.MSG);
            }
            
            return SDFileViewModel;

        }
        /*Creates Compound (view-model) from string */
        private CompoundViewModel CreateCompoundViewModel(string compoundString)
        {
            return ViewModelFactory.CreateCompoundViewModel(compoundString);
        }

        /*Parses "SDFile" to JSON and save it in "pathFile"*/
        public void ToJSONFile(string pathFile)
        {
            string json = "[\r\n";
            foreach(CompoundViewModel c in SDFileViewModel.CompoundViewModels)
            {
                json += "  {\r\n    \"Structure\": \"" + c.MolFileStructure.Replace("\r\n", "\\r\\n") + "\"";
                foreach(DataItem d in c.Properties)
                {
                    json += ",\r\n    \"" + d.Property + "\": " + "\"" + d.Value + "\"";
                }
                json += "\r\n  },\r\n";
            }
            json = json.Substring(0,json.Length-3) + "\r\n";
            json += "]\r\n";
            System.IO.File.WriteAllText(pathFile, json);
        }
    }
}