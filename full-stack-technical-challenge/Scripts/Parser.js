﻿$(document).ready(function () {
    var recordsDataJSONFormat;

    $('[data-toggle="tooltip"]').tooltip();

    //Shows the list of records
    populateRecordsList = function () {
        var html = "";
        $("#recordsList").prop('html', html);
        $.each(recordsDataJSONFormat.CompoundViewModels, function (i,item) {
            html += '<a  id="' + i + '" name="' + item.ID + '" class="list-group-item">' + item.Name + '-' + item.ID + '<span class="badge"  data-placement="right" data-toggle="tooltip" title="Attributes">' + item.PropertiesCount + '</span></a>';
            $("#recordsList").html(html);
        });
        bindEventToAnchorTags();
        
    }

    
    //Shows the list of attributes of the selected record
    populatePropertiesTable = function (sR, pattern = undefined) {
        var html = "";
        html += '<div class="table-responsive"><table class="table table-striped table-bordered"><thead><tr><th>Property</th><th>Value</th></tr></thead><tbody>';
        $.each(sR.Properties, function (i, item) {
            if (pattern != undefined) {
                if (item.Value.search(pattern) != -1)
                    html += '<tr><td>' + item.Property + '</td><td>' + item.Value + '</td></tr>';
            } else {
                html += '<tr><td>' + item.Property + '</td><td>' + item.Value + '</td></tr>';
            }
            
        });
        html += '</tbody></table></div >';
        $("div#propertiesContainer").html(html);
    }

    //Shows the selected record graphically
    showRecordImage = function (sR) {
        formdata = new FormData();
        data = '{"chemDataType": "chemical/x-mdl-molfile","imageType": "image/png","chemData": "' + sR.MolFileStructure.replace(/\n/g, "\\n").replace(/\r/g, "\\r") + '"}';
        formdata.append("data", data);

        jQuery.ajax({
            url: "Parser/GetImage",
            type: "POST",
            data: formdata,
            processData: false,
            contentType: false,
            success: function (data) {
                $('div#imageContainer img').prop('src', 'data:image/png;base64,' + data);
            },
            error: function (result) {                
                alert(JSON.stringify(result));
            }
        });
       
    }

    //Clears all components
    clearData = function () {
        $('div#imageContainer img').prop('src', '');
        $("div#propertiesContainer").html('');
        $("#recordsList").html('');
        selectedRecord = undefined;
        $("button#search").prop("disabled", true);
        $("input#pattern").val('');

    }

    //shows only the attributes that match with pattern in search input
    $("button#search").click(function () {
        populatePropertiesTable(selectedRecord, $("input#pattern").val());
    });

    //change disabled property of search button taking into account tha value of search input.
    $("input#pattern").on('input', function () {
        $("button#search").prop("disabled", this.value.length == 0 || selectedRecord == undefined);
        if (this.value.length == 0 && selectedRecord != undefined)
            populatePropertiesTable(selectedRecord);
    });


    //change disabled property of submitFile button taking into account tha value of inputFile input.
    $("#inputFile").change(function () {
        $("#submitFile").prop("disabled", this.files.length == 0);
        if (this.files[0] == undefined || this.files[0] != selectedFile) {
            clearData();
        }
    });

    //Stores user's selected record
    var selectedRecord;

    //Binds click event for each property (<a> tag).
    bindEventToAnchorTags = function () {
        $("div#recordsContainer a").click(function () {
            if (selectedRecord != recordsDataJSONFormat.CompoundViewModels[this.id]) {
                selectedRecord = recordsDataJSONFormat.CompoundViewModels[this.id];
                populatePropertiesTable(selectedRecord);
                showRecordImage(selectedRecord); 
            }

        });
    }
    //Stores selected file
    var selectedFile;

    //Uploads selected SD File and parse it
    $("#submitFile").click(function () {
        
        file = $("#inputFile").prop('files')[0];
        if (file != undefined && file != selectedFile) {
            selectedFile = file;
            formdata = new FormData();
            formdata.append("inputFile", file);
            jQuery.ajax({
                url: "Parser/Parse",
                type: "POST",
                data: formdata,
                processData: false,
                contentType: false,
                success: function (data) {
                    if (data == "Please select a valid file" || data == "The selected SD File does not have a valid format.")
                        alert(data);
                    else{
                        recordsDataJSONFormat = data;
                        populateRecordsList();
                    }                    
                },
                error: function (result) {
                    alert(result);
                }
            });
        } else {
            alert("Please select another SD File.");
        }
        
    });
});