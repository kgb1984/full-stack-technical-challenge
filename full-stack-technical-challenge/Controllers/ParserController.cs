﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using full_stack_technical_challenge.BusinessLogic;
using full_stack_technical_challenge.Models;
using full_stack_technical_challenge.ViewModel;
using Newtonsoft.Json;

namespace full_stack_technical_challenge.Controllers
{
    //Controller to parse SDFile to JSON File
    public class ParserController : Controller
    {
        public Parser Parser { get; set; }


        /*Action for parsing SD File (inputFile)
         The final JSON file will be stored  in JSONFile  
         folder with the same name of the original file (i.e. sample_1.sdf will be saved as sample_1.json) 
        */
        [HttpPost]
        public JsonResult Parse(HttpPostedFileBase inputFile)
        {
            var jsonResult = "";
            try
            {
                if (!Path.GetFileName(inputFile.FileName.ToLower()).EndsWith(".sdf"))
                {
                    jsonResult = "Please select a valid file";
                }
                else
                {
                    string sdFilePath, jsonFilePath;
                    //A different implementation for testing purpose.
                    if (Server == null)
                    {
                        sdFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\TestFiles\" + Path.GetFileName(inputFile.FileName));
                        jsonFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, @"..\..\TestFiles\" + Path.GetFileName(inputFile.FileName)).Replace(".sdf", ".json");
                    }
                    else
                    {
                        sdFilePath = Server.MapPath("~/SDFiles/" + Path.GetFileName(inputFile.FileName));
                        jsonFilePath = Server.MapPath("~/JSONFiles/" + Path.GetFileName(inputFile.FileName)).Replace(".sdf", ".json");
                        inputFile.SaveAs(sdFilePath);
                    }
                    
                    
                    Parser = new Parser(sdFilePath);
                    Parser.ToJSONFile(jsonFilePath);
                    return Json(Parser.SDFileViewModel);
                }
            }
            catch (Exception ex)
            {
                jsonResult = ex.Message;
            }  
            return Json(jsonResult);
        }

        //Obtains graphical representation of a record from MOL text (data)
        [HttpPost]
        public JsonResult GetImage(string data)
        {
            string jsonResult;
            try
            {
                string reqUrl = "https://chemdrawdirect.perkinelmer.cloud/rest/generateImage";

                var httpWebRequest = (HttpWebRequest)WebRequest.Create(reqUrl);
                httpWebRequest.ContentType = "application/json; charset=utf-8";
                httpWebRequest.Method = "POST";

                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(data);
                    streamWriter.Flush();
                }
                var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
                if (httpResponse.StatusCode != HttpStatusCode.OK)
                    throw new Exception(String.Format("Server error (HTTP {0}: {1}).",httpResponse.StatusCode,httpResponse.StatusDescription));
                using (var memoryStream = new MemoryStream())
                {
                    httpResponse.GetResponseStream().CopyTo(memoryStream);
                    Bitmap image = new Bitmap(1, 1);
                    image.Save(memoryStream, ImageFormat.Png);

                    byte[] byteImage = memoryStream.ToArray();
                    string imageBase64Data = Convert.ToBase64String(byteImage.ToArray());
                    jsonResult = imageBase64Data;
                }
            }
            catch (WebException ex)
            {
                jsonResult = ex.Message;
            }
            return Json(jsonResult);
        }

    }
}
